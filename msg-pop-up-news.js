// NEWSLETTER - MENSAGEM EM POP-UP
function insertMailAjax(form){
	form = $(form);
	$.ajax({
		url: form.attr('action'),
		data: form.serialize(),
		type: 'post',
		success: function(){
			showMessage(form.find('[name="message-success"]').val());
		}
	})
}
