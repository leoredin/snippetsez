// PAGINA DE PRODUTO - Calculo de Frete
    $('#interactivity').append('<div id="resultado"></div>');
    $('#resultado').before('<span class="loading" style="display: none;">Aguarde estamos calculando...</span>');
    $('#resultado').css({'display':'none'});
    
    $('#interactivity a#shipping').click(function(e){
           
        e.preventDefault();
         
        $('#sb-container').css({'display':'none'});
      
        $('#resultado').hide();
        $(this).html('...').css({'pointer-events':'none', 'cursor':'default'});
        $('#interactivity .loading').show();
        
        var href = $(this).attr('href');
        var precoProduto = $('.sale-price b.sale').html().replace('R$','').replace(',','.');
        var cepAtual = $('.shipping-cep').val().replace('-','');
    
        function getUrlVars() {
            var vars = [], hash;
            var hashes = href.slice(href.indexOf('?') + 1).split('&');
            for(var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }
            
        $.ajax({
            url: '/Popup_Simular_Frete_Xp.asp',
            type: 'post',
            data: {
                InternalReferer: "1",
                acao: 'calcular',
                produtoid: getUrlVars()['produtoid'],
                varid: $('input[name*="varid_"]:checked').val(),
                subvarid: $('input[name*="subvarid_"]:checked').val(),
                peso: "0",
                produto: "",
                preco: precoProduto,
                cep: cepAtual,
                quantidade: 1
            }
        }).done(function(data) {
                var html='';
                if(!data.records||!data.freights.length){
                  html='<table width="100%" border="0" cellpadding="4" cellspacing="0">' +'<tr>' +'<td align="center" bgcolor="#F4F4F4" class="b preto2 a8"><strong>Nenhuma Forma de Entrega Encontrada!</strong></td>' +'</tr>' +'<tr>' +'<td align="center">Por favor,entre em contato com a nossa central de atendimento pelo telefone ou pelo e-mail.</td>' +'</tr>' +'</table>';
                }else{
                    html+='<table width="100%" border="0" cellpadding="0" cellspacing="0" id="freights-table" style="margin-top:10px; border-collapse:collapse">' +'<tr>' +'<td colspan="3" class="head-title">' +'<strong>Formas de entrega disponíveis para:</strong> ';
                    if(data.ShippingAddress.CEPIsValid=='true') {
                        html+=data.ShippingAddress.Address+' - '+data.ShippingAddress.Neighborhood+' - '+data.ShippingAddress.City+' - '+data.ShippingAddress.State;
                    }else{
                        html+='<strong class="highlight">' +(data.ShippingAddress.State.length?data.ShippingAddress.State:'') +(data.ShippingAddress.TypeCEP.length?(data.ShippingAddress.TypeCEP=='I'?' - Interior':' - Capital'):'') +'</strong>';
                    }
                    html+='<br>' +'</td>' +'</tr>';
                    for(var i=0; i < data.freights.length; i++){
                        var freight=data.freights[i];
                        var timeHtml='';
                        var img='';
                        freight.freightAmount=parseFloat(freight.freightAmount);
                        if(data.showDeliveryTime){
                            timeHtml+='<td width="70%" class="td-time">' +freight.freightText;
                            if(freight.freightHasEstimateTag.toString().toLowerCase()=='false'&&freight.deliveryTime > 0){
                                timeHtml+='<span class="objETA">' +'<b>Prazo para Entrega:</b> '+freight.deliveryTime+' ' +(freight.deliveryTime > 1?'Dias úteis':'Dia útil')+'.' +'</span>';
                            }
                            timeHtml+='</td>';
                        }
                    if(freight.image){
                        img='<img id="image-fe-'+freight.freightId+'" onerror="removeImageError(this.id);" '+'src="/FILES/_ENTREGAS/'+freight.image+'" alt="'+freight.freightName+'">';
                    }else{
                        var freightImgTxt=(freight.freightName||'').replace('Transportadora','Transp.');
                        var freightImgTxt=(freightImgTxt.length > 16?freightImgTxt.substr(0,16)+'...':freightImgTxt);
                        img='<table width="70" class="no-img-table" border="0" cellspacing="0" cellpadding="0">' +'<tr>' +'<td height="30" align="center" valign="middle">'+'<strong>'+freightImgTxt+'</strong>' +'</td>' +'</tr>' +'</table>'
                    }
                    html+='<tr id="freight-'+freight.freightId+'">' +'<td width="1" class="td-image" valign="top">' +img +'</td>' +'<td width="'+(data.showDeliveryTime?'30%':'100%')+'" class="td-amount">' +(freight.freightAmount > 0?'<strong class="currency">'+formatCurrency(freight.freightAmount)+'</strong> - ':'') +'<strong>'+freight.freightName+'</strong>' +'<br>' +(!data.showDeliveryTime?freight.freightText:'') +'</td>' +timeHtml +'</tr>';
                }
            html+='</table>';
            }
            $('#interactivity .loading').hide();
            $('#resultado').html(html);
            $('#resultado').show();
            $('#shipping').html('OK').css({'pointer-events':'auto', 'cursor':'auto'});
        });
    });