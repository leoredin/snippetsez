function roundToNearest(numToRound, numToRoundTo) {
    return Math.round(numToRound / numToRoundTo) * numToRoundTo;
}

$(".largura-quantidade input").on("change", function() {
    let valorCentimetros = $(this).val();
    let round = roundToNearest(valorCentimetros, 10);
    $(this).val(round);
});
