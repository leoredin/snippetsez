(function(body) {
    if (body.attr("id") != "page-produtos") {
        return;
    } else {
        if ($("dd.brand a").length) {
            var aBrand = $("dd.brand a");
            var id = aBrand.attr("href").match(/m[0-9]+/gi);
            id = id != null ? id[0].replace(/[^0-9]/gi, "") : 0;
            if (id > 0) {
                aBrand
                    .css({
                        width: "160px",
                        height: "60px",
                        display: "block",
                        "text-align": "center"
                    })
                    .before(
                        '<span class="brand-title">Clique na logo e confira todos os produtos desta marca</span>'
                    )
                    .append(
                        '<img class="lazyload" src="/FILES/_IMG/MARCAS/M' + id + '.jpg" >'
                    );
            }
        }
    }
})($("body"));
