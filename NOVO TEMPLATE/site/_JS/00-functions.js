﻿$(function(){


	//MENU HORIZONTAL - NIVEL 2 - AJUSTA LARGURA
	$('#sections-menu-horizontal .level-1 > li').each(function(){
		$(this).mouseenter(function(){
			var wrapperMenu	= $(this).parents('.wrapper').width();
			var menuPos		= $(this).position().left;
			var menuWidth	= $(this).find('.level-2').outerWidth();
			var totalSize	= menuWidth + menuPos;
			if ( totalSize > wrapperMenu ){
				$(this).find('.level-2').css('margin-left', wrapperMenu - totalSize);
			}
		});
	});



	// CARRINHO: ALTERA O LINK DE CHAT DO RODAPE NO CARRINHO
	$('.check-out .help a.person').attr({'href': '/fale-conosco/'});


	// CORRIGE Botao cadastrar painel de controle
	$('.field-wrapper .cep-wrapper').append($('#nacional'));


	//VITRINES: MOVE CONTEUDO
	$('.products li').each(function(){
		$(this).find('.label').wrapAll('<div class="labels-wrapper"/>');
		$(this).find('.labels-wrapper').appendTo($('.photo', this));
		$(this).find('.buttons').appendTo($('.product-info', this));
		$(this).find('.product-name').insertBefore($('.price', this));
	});


	//MENU - TODAS CATEGORIAS
	$('.todas-categorias ul.level-1 > li:nth-child(5n)').after('<span class="clear-both"></span>');


	//PAGINA DE PRODUTO - MOVE THUMBS SE NAO HOUVER COLUNAS
	$('#page-produtos .without-left-column.without-right-column .product-photos').prependTo('#media .media-wrapper');

	//PAGINA DE PRODUTO - MOVE LINKS
	$('#page-produtos .product-service').insertBefore('#product-social-networks');


	//PAGINA DE PRODUTO - FRETE - apenas quando ativo o campo para digitar o cep
	if($('#page-produtos input.shipping-cep').length > 0 ) {		
		$('#interactivity .product-service').after('<span class="custom-frete"><span class="custom-frete-wrapper"><span class="icon"></span><span class="title">Calcular frete e prazo</span><a class="nao-sei-cep" rel="shadowbox;width=640;height=385" href="Popup_Busca_Cep.asp?campo=.shipping-cep">Não sei meu CEP</span></span></span>');
		$('#page-produtos input.shipping-cep').appendTo('#interactivity .custom-frete-wrapper');
		$('#interactivity a#shipping').appendTo('#interactivity .custom-frete-wrapper');
		$('#page-produtos a#shipping').removeClass('btn-alt').html('OK');
		$('a#payments').removeClass('btn-alt').appendTo('.product-service .content'); /* move botao saiba como pagar */
	}



	//COMPRE JUNTO - coloca nome abaixo
	$('ul.buy-together-list > li').each(function(){
		$(this).find('.buytogether-content a.product-title:nth-child(5)').appendTo($('li.buytogether-item:nth-child(5) > a', this));
		$(this).find('.buytogether-content a.product-title:nth-child(3)').appendTo($('li.buytogether-item:nth-child(3) > a', this));
		$(this).find('.buytogether-content a.product-title:nth-child(1)').appendTo($('li.buytogether-item:nth-child(1) > a', this));
	});
	//COMPRE JUNTO - SE HOUVER 3 PRODUTOS
	$('#buytogether-v2 li li:nth-child(5)').parent().addClass('tres-produtos');


	//RODAPE - CENTRAL DE ATENDIMENTO - VAZIO
	$('.header-atendimento ul li:empty, .footer-atendimento ul li:empty').css('display','none');
	$('.header-atendimento ul li b:empty').parent().css('display','none');
	


	// ADD CLASS "ESGOTADO"
	$('.products .price a').each(function(){
		var textBtn = $(this).text();
		if( textBtn == 'Esgotado' ) {
		$(this).addClass('esgotado');
		}
	});


});



//TOOLBAR
$(window).load(function(){

	if($('.header-1').length > 0){
		$(window).bind('scroll',function(){
			if ( $(this).scrollTop() > 30 ) {
	  			$('.header-1').addClass('open');
			} else {
	  			$('.header-1').removeClass('open');
			}
		});
	}



});



// NEWSLETTER - MENSAGEM EM POP-UP
function insertMailAjax(form){
	form = $(form);
	$.ajax({
		url: form.attr('action'),
		data: form.serialize(),
		type: 'post',
		success: function(){
			showMessage(form.find('[name="message-success"]').val());
		}
	})
}

