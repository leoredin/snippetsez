jQuery('<div/>', {
    id: 'slider',
}).appendTo('#center article');

$('article .component-group').each(function () {
    if ($(this).find('.carousel').length) {
        $(this).find('.legend').clone().appendTo('#slider');
        $(this).find('.products').clone().appendTo('#slider');
        $(this).remove();
    }
})

$('#slider ul').replaceWith(function () {
    return $('<div/>', {
        html: $(this).html(),
        class: 'slider-custom'
    });
});

$('.slider-custom li').replaceWith(function () {
    return $('<div/>', {
        html: $(this).html()
    });
});

$('.slider-custom').slick({
    arrows: true,
    prevArrow: "<button type='button' class='slick-prev pull-left'><i class='fa fa-angle-left' aria-hidden='true'></i></button>",
    nextArrow: "<button type='button' class='slick-next pull-right'><i class='fa fa-angle-right' aria-hidden='true'></i></button>"

    /* DOCUMENTAÇÃO = > http://kenwheeler.github.io/slick/ */
});