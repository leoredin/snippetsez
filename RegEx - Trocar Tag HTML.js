var replacementTag = 'div';

$('.promocao li').each(function() {
        var outer = this.outerHTML;

        // Replace opening tag
        var regex = new RegExp('<' + this.tagName, 'i');
        var newTag = outer.replace(regex, '<' + replacementTag);

        // Replace closing tag
        regex = new RegExp('</' + this.tagName, 'i');
        newTag = newTag.replace(regex, '</' + replacementTag);

        $(this).replaceWith(newTag);
        $(this).appendTo('#slider')
});