(function($,window,document,undefined) {
	var options = {},
		content,
		defaults = {
			autoLoad: true,
			sensitive:0,
			page: 1,
			pageCount: 1,
			url: location.href,
			param: 'page',
			content: '.content',
			start: function() {return this},
			load: function() {return this},
			inloading:false
		};

	$.autopager = function(options) {
		var x = this.autopager;
		x.options = options = $.extend({}, defaults, options);
		x.start = function() {options.start();return this};
		x.load = function() {options.load();x.inloading=false;return this};

		var content = $(options.content+':last');
		if(content.length && options.pageCount>1){
			if(typeof(_gaq)!=='undefined'){
				_gaq.push(['_setAccount', cfg.GoogleAnalytics]);
			}
			$(window).bind('scroll',function(){
				if(!x.inloading){
					var content = $(options.content+':last');
					if ((content.offset().top + content.height() < $(window).scrollTop() + $(window).height()) && x.options.page<x.options.pageCount) {
						x.start();
					}
					if (((content.offset().top + content.height()+options.sensitive) < $(window).scrollTop() + $(window).height()) && x.options.page<x.options.pageCount) {
						x.next();
					}
				}
			}).scroll();
		}
		return this;
	};

	$.extend($.autopager, {
		next:function(){
			var x = this;
			var re = new RegExp(x.options.param+'\=[0-9]*','gi');
			var nextUrl = x.options.url.replace(re,'').replace(/\?\&/gi,'?').replace(/\&{2,}/gi,'').replace(/\&$/gi,'').replace(/\?$/gi,'').replace(/\&[^\=]*$/gi,'');
			nextUrl = nextUrl + (nextUrl.indexOf('?')>-1 ? '&' : '?') + x.options.param + '=' + (x.options.page+1);
			$.ajax({
				url:nextUrl,
				async:true,
				beforeSend:function(){x.inloading=true},
				success:function(data){
					x.options.page = x.options.page+1;	
					var newContent = $(data).find(x.options.content).hide();
					$(x.options.content+':last').after(newContent);
					newContent.fadeIn('slow');
					if(typeof(_gaq)!=='undefined'){
						_gaq.push(['_trackPageview',nextUrl]);
					}
				},
				complete:x.load
			});
		}
	});

})(jQuery,window,document);

// call
(function($,window,document,undefined) {
	$(function(){
		if(!$('#page-produtos-secoes,#page-produtos-pesquisa').length){return;}
		var page,pageCount;
		page = parseFloat($('#center-header .results span:eq(0)').text());
		pageCount = parseFloat($('#center-header .results span:eq(1)').text());
		$('#center-header .results, #center-header .page-number, #center-footer .page-number').hide();
		var loading = $('<div style="text-align:center;padding:10px;clear:both;"><img src="/files/_img/loading.gif"></div>');
		$.autopager({
			sensitive:70,
			page:page,
			pageCount:pageCount,
			param:'pagina',
			content:'ul.products',
			start:function(){
				$('ul.products:last').after(loading);
			},
			load:function(){
				var imgsLazy = $("img.lazyload");
				if(imgsLazy.length>0){imgsLazy.lazy(window.lazyConfig);}
				loading.remove();
				$(window).resize();
				Shadowbox.clearCache();
				Shadowbox.setup();
				
			}
		});
	});
})(jQuery,window,document);