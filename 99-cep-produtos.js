//PAGINA DE PRODUTO - FRETE - apenas quando ativo o campo para digitar o cep
	if($('#page-produtos input.shipping-cep').length > 0 ) {		
		$('#interactivity .product-service').after('<span class="custom-frete"><span class="custom-frete-wrapper"><span class="icon"></span><span class="title">Calcular frete e prazo</span><a class="nao-sei-cep" rel="shadowbox;width=640;height=385" href="Popup_Busca_Cep.asp?campo=.shipping-cep">Não sei meu CEP</span></span></span>');
		$('#page-produtos input.shipping-cep').appendTo('#interactivity .custom-frete-wrapper');
		$('#interactivity a#shipping').appendTo('#interactivity .custom-frete-wrapper');
		$('#page-produtos a#shipping').removeClass('btn-alt').html('OK');
		$('a#payments').removeClass('btn-alt').appendTo('.product-service .content'); /* move botao saiba como pagar */
	}