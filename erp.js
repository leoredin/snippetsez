let i = 0;                                            //Variavel para varrer o json (implementar contador)  this.json.value.length retorna a quantidade de skus
let JsonMillenium = {                                 // Objeto contendo dados preformatados para post na layer via ajax

          "SKU": this.json.value[i].sku,             //propriedade Sku Do Millennium
          "integrationID": this.json.value[i].ID,    //Propriedade ID do Millennium
          "WarehouseID": 1,                          //Warehouse a definir
          "Quantity": this.json.value[i].saldo,      //Estoque No Millennium
          "InventoryMovementTypes":2,                //Tipo de movimentacao de estoque na layer do core (recontagem)
          "ActionEnum":"2"                           //Tipo de movimentacao de estoque na layer do core (recontagem)
 	}

 $.ajax({                                                
    url: '/v1/Inventory/API.svc/web/ChangeSKUInventory',    //url do post
    type: 'post',                                           //metodo post
    data: JSON.stringify({                                  //transforma Json em texto para ser lido pelo Ajax
              JsonMillenium
      }),
    beforeSend: function(xhr) {                    
        xhr.setRequestHeader('Content-Type', 'application/json');
        xhr.setRequestHeader('Accept', 'application/json');
    },
    success: function(r) {
        console.log(eval(r));
    }
})