$('#est_professor').parent().addClass('est_professor');
$(".extended-fields .field-wrapper:not(.est_professor)").addClass('campos-professores');

$('select#est_professor').on('change', function () {
	if ($(this).val() == 1) {
		$('.extended-fields .field-wrapper:not(.est_professor) input').attr('required', true);
		$('.extended-fields .field-wrapper #est_observacoes input').removeAttr('required');
		$('<i class="required-field">*</i>').prependTo('.extended-fields .field-wrapper:not(.est_professor) label');
		$('.extended-fields .field-wrapper label[for=est_observacoes] i.required-field').remove();

		$('.extended-fields .field-wrapper:not(.est_professor)').removeClass('campos-professores');
	} else {
		$('.extended-fields .field-wrapper:not(.est_professor) input').removeAttr('required');
		$('.extended-fields .field-wrapper:not(.est_professor) label i.required-field').remove();

		$('.extended-fields .field-wrapper:not(.est_professor)').addClass('campos-professores');
	}
})

$('input#est_livrosocilitado').after('<span class="livros-disponiveis"> Apenas livros da Livraria do Advogado </span>');

/* CSS
.livros-disponiveis {
    color: red;
}
.campos-professores{
	display: none;
}
.extended-fields {
    min-height: 0px !important;
}
*/