var continueOrCartSettings = {
	successMessage: "O Produto foi adicionado ao carrinho!",
	modalWidth: 400,
	modalHeight: 275
};

var continueOrCart = {

	productHTML:	'<div class="product-info">' +
						'<img src="#IMG#" alt="#NAME#" onerror="$(this).attr(\'src\',\'/files/_fotos/semfoto.gif\');" />' +
						'<span class="t-name">#NAME#</span>' +
						'<span class="t-price">#PRICE#</span>' +
					'</div>',

	defaultHTML:	'<div class="wrapper cart-message-wrapper">' +
						'<p class="msg cart-message">#CART_MESSAGE#</p>' + 
						'#HTML_PRODUCT#' +
						'<div class="button-wrapper">' + 
							'#HTML_BUTTONS#' +
						'</div>' + 
					'</div>',

	tempHTML: 		'',

	loadingHTML:	'<div id="sb-loading-inner"><span>Carregando...</span></div>',

	IsAdding: 		false, 

	product: {
		loaded: false,
		img: '',
		name: '',
		price: ''
	},

	addProduct: function(settings){

		var settings = settings || {};

		if (continueOrCart.IsAdding) {
			continueOrCart.IsAdding = false;
			return false;
		}

		var frm 		= $('form[name="buy"]'),
			btnBuy		= '#btn-buy';
		if ($('#form-kit').length){
			frm 	= $('#form-kit');
			btnBuy	= '#btn-buy';
		}
		
		if((!$(btnBuy).length) || (!frm.valid())){
			return false;
		}
		
		continueOrCart.IsAdding = true;

		continueOrCart.loadProductInfo();

		// se a popup jÃ¡ estiver aberta
		var sbRenderTime1 = 0,
			sbRenderTime2 = 1500;
		if(Shadowbox.isOpen()){
			sbRenderTime1 = 1000;
			sbRenderTime2 = 3000;
			Shadowbox.close();
		}

		setTimeout(function(){
			Shadowbox.open({
				player:		'html',
				content: 	continueOrCart.loadingHTML,
				height:     settings.modalHeight || 275,
				width:      settings.modalWidth || 400
			});
		}, sbRenderTime1);
			


		var callbackSendForm = function(cartData){
			var strMessage, strButtons, strProduct = '';
			if (cartData.IsValid) {
				strMessage = settings.successMessage || 'Produto adicionado com sucesso!';
				strButtons = '<label for="back" class="close-btn"><a href="javascript:void(0);" onclick="Shadowbox.close();" target="_top" class="btn">Continuar comprando</a></label>';
				strProduct = continueOrCart.productHTML;
				strProduct = strProduct.replace(/#NAME#/g, unescape(continueOrCart.product.name));
				strProduct = strProduct.replace(/#IMG#/g, continueOrCart.product.img);
				strProduct = strProduct.replace(/#PRICE#/g, continueOrCart.product.price);
			} else {
				strMessage = continueOrCart.searchCartError();
				strButtons = '<label for="close" class="close-btn"><a href="javascript:void(0);" onclick="Shadowbox.close();" target="_top" class="btn">Fechar</a></label>';
			}

			strButtons 	   = strButtons + 
							 '<label for="go-to-cart" class="go-btn"><a href="/meu-carrinho/" onclick="continueOrCart.loadingShadowbox();" target="_top" class="btn">Ir ao carrinho</a></label>';

			var strHTML = continueOrCart.defaultHTML;
				strHTML	=	strHTML.replace('#CART_MESSAGE#', strMessage);
				strHTML	=	strHTML.replace('#HTML_PRODUCT#', strProduct);
				strHTML	=	strHTML.replace('#HTML_BUTTONS#', strButtons);
			continueOrCart.tempHTML = strHTML;

			setTimeout(function(){
				$('#sb-player').html(continueOrCart.tempHTML);
			}, sbRenderTime2);

			if(window['sessionReload']){
				sessionReload();
			}

			continueOrCart.IsAdding = false;
		};

		continueOrCart.sendProductFormData(frm, callbackSendForm);
		return false;
	},

	sendProductFormData: function(frm, callback){
		var oResult = {},
			fAction = frm.attr('action');
		$.ajax({
			url: fAction,
			xhrFields: {
				withCredentials: true
			},
			crossDomain: true,
			data: frm.serialize() + '&ReturnResult=1',
			type: 'POST',
			success: function(data){
				if(typeof(data)!='object'){
					eval('oResult = '+data+';');
				} else {
					oResult = data;
				}
				callback(oResult);
			},
			error: function(){
				callback({"IsValid":false});
			}
		});
	},

	searchCartError: function(){
		var oError = {},
			strReturn = '';
		$.ajax({
			url: '/Carrinho_Erro.asp',
			async: false,
			type: 'GET',
			success: function(data){
				if(!typeof(data)=='object'){
					eval('oError = '+data+';');
				} else {
					oError = data;
				}
			}
		});
		if (oError.description){ 
			strReturn = oError.description;
		} else {
			strReturn = 'Desculpe, nÃ£o foi possÃ­vel adicionar este produto ao carrinho.';
		}
		return strReturn;
	},

	loadProductInfo: function(){
		if(!continueOrCart.product.loaded){
			continueOrCart.product.name = escape($('.product-description dl dt[itemprop="name"]').text());
			continueOrCart.product.price = '<div class="price">' + $('.purchase-info .price').html() + '</div>';
			continueOrCart.product.img = $('#img-product').attr('src');
			continueOrCart.product.loaded = true;
		}
	},

	loadingShadowbox: function(){
		$('#sb-player').html(continueOrCart.loadingHTML);
	}

};

$(function(){
	if ($('#page-produtos').length && !$('body').hasClass('mobile')) {
		$('form[name="buy"], #form-kit').unbind('submit').bind('submit', function(e){
			continueOrCart.addProduct(continueOrCartSettings);
			e.preventDefault();
			return false;
		}).attr('onsubmit', 'return false');
	}
});