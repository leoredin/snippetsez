﻿<%

' Nesse arquivo:
' * Vitrine_Layout_1 (layout-1) Foto: Nome abaixo e Preço abaixo
' * Vitrine_Layout_2 (layout-2) Foto: Nome ao lado e Preço ao lado
' * Vitrine_Layout_3 (layout-3) Foto: Nome ao lado e preço ao lado + Veja Também
' * Vitrine_Layout_4 (layout-4) Foto Grande: Nome ao lado e Preço ao lado
' * Vitrine_Layout_5 (layout-5) Carrousel de Produtos

' ===========================================
' .layout-1: Foto: Nome abaixo e Preço abaixo
' ===========================================
Class Vitrine_Layout_1_Advogado
	Public Function RetornarHTML(rs)
		Dim HTML, ProdutoID, Url, NomeProduto, Preco, Descricao, Foto, Rating, strClass
		Set Flag = new Produtos_CamposEstendidos
		HTML = "<ul class=""products"">"
		While Not rs.EOF
			ProdutoID		= rs("ProdutoID")
			Url				= RetornarURLFriendly("p", rs("NomeDoProduto"), ProdutoID, "")
			NomeProduto		= HTMLEncode(rs("NomeDoProduto"))
			Preco			= MontaPrecoProduto(zero(rs("DisponivelParaCompra")), rs("Condicoes_Descricao"), rs("ProdutoID"), True, "") 'rs("Condicoes_Descricao")
			Descricao		= StripAllHTMLTags(Decodificar(rs("BreveDescricao")))
			If Len(Descricao) > 100 Then
				Descricao = Left(Descricao, 100) & " &hellip;"
			End If
			Foto			= GetFotoListagem(rs, 1)
			Foto			= IIF(Foto="", cfgHostImages(0)&"files/_fotos/semfoto.gif", Foto)
			Rating			= rs("Avaliacao")
			strClass		= ""
			If (Cstr(cfgControleDeEstoque)="1" And Not Cbool(zero(rs("DisponivelParaCompra")))) Then
				Preco	= "<a href=""popup_disponibilidade.asp?produtoid="&rs("ProdutoID")&""" data-modal=""shadowbox;width=300;height=360"" title=""Clique aqui para ser avisado quando este produto estiver dispon&iacute;vel"" class=""esgotado"">Esgotado</a>"
				strClass	=	"sold-out"
			End If

			ProdVisibilidade = True
			If rs("visualiza_preco") Then 
				If Session("Logado") <> "SIM" Then 				
					ProdVisibilidade = False
				End If 
			End If

			prevenda = ""
			If rs("prevenda") And rs("PreVendaData") >= Now Then
				prevenda = "<div class=""pre-ordable"">Pré-Venda até "&rs("PreVendaData")&"</div>"
			End If

			HasVariation = IIF(rs("HasVariation"),"1","0")
			HasSpecialAttribute = IIF(rs("HasSpecialAttribute"),"1","0")
			
			'SEÇÃO DO PRODUTO
			CategoriaProduto	= rs("SecaoDesc")
			CategoriaUrl		= RetornarURLFriendly("s", rs("SecaoDesc"), rs("SecaoId"), "")
			CategoriaHTML		= "<a href="""&CategoriaUrl&""" title=""Ver mais produtos da categoria "& CategoriaProduto &""">"&CategoriaProduto&"</a>"
			
			HTMLSold	=	"<div id=""products-sold-"&ProdutoID&""" class=""products-sold"">"&vbcrlf&_
								"<div class=""products-sold-header"">"&IIF(cfgExibirVendasProdutoLegend <> "",cfgExibirVendasProdutoLegend, "Vendidos")&"</div>"&vbcrlf&_
								"<span class=""products-sold-content"">"&rs("VendasEditavel")&"</span>"&vbcrlf&_
								"<div class=""products-sold-footer"">"&IIF(cfgExibirVendasProdutoFooter <> "",cfgExibirVendasProdutoFooter," ")&"</div>"&vbcrlf&_
							"</div>"

			strHTMLFLags = Flag.RenderizarFlags(rs,null,rs("produtoid"))

			HTML = HTML &_
				"<li id=""li-productid-"&ProdutoID&""" class=""P"&ProdutoID&" "&strClass &""">"&_
					"<input type=""hidden"" name=""ApresentacaoAlias"" value="""& rs("ApresentacaoAlias") &""">"&_
					"<input type=""hidden"" name=""HasVariation"" value="""& HasVariation &""">"&_
					"<input type=""hidden"" name=""HasSpecialAttribute"" value="""& HasSpecialAttribute &""">"&_
					"<div class=""product"">"&_
						"<div class=""product-name""><a href="""&Url&""" title="""&NomeProduto&""">"&NomeProduto&"</a></div>"&_
						"<div class=""product-autor"">"&rs("est_autor")&"</div>"&_
						"<a class=""photo"" href="""&Url&""" title="""&NomeProduto&"""><img class=""lazyload"" src=""/FILES/_IMG/spacer.gif"" data-src="""&Foto&""" data-gallery="""&rs("Foto")&""" title="""&NomeProduto&""" width="""&cfgLargura_Foto_Pequena&""" height="""&cfgAltura_Foto_Pequena&""" alt="""&NomeProduto&"""></a>"&_
						RenderVariationsList(rs, ProdutoID, ObjVariationsList, LastProductControl)&_
						"<span class=""rating rate-"&Rating&"""></span>"&_
						IIF(Descricao<>"", "<div class=""description""><a href="""&Url&""" title="""&Descricao&""">"&Descricao&"</a></div>", "")&_
						prevenda &_
						IIF(ProdVisibilidade,"<div class=""price"">"&Preco&"</div>","")&_
						IIF(rs("fretegratis"),"<span class=""label free-shipping"" title=""Frete Gr&aacute;tis""><b>Frete Gr&aacute;tis</b></span>","")&_
						IIF(rs("promocao") AND rs("promocaoinicio") < NOW AND rs("promocaofim") > NOW,"<span class=""label promotion"" title=""Promo&ccedil;&atilde;o""><b>Promo&ccedil;&atilde;o</b></span>","")&_
						"<div class=""more-sections""><p>Veja mais</p>"&_
							"<span class=""section""><b>+</b>"&CategoriaHTML&"</span> "&_
							"<span class=""brand""><b>+</b><a href="""&RetornarUrlMarca(rs)&""" title=""Ver mais produtos da marca "&rs("MarcaDesc")&""">"&rs("MarcaDesc")&"</a></span> "&_
						"</div>"&_
						strHTMLFLags&_
						IIF(rs("ExibirVendasProduto"),HTMLSold,"")&vbcrlf&_
						"<div class=""buttons"">"&_
							"<a class=""details"" href="""&URL&""" title=""Ver detalhes""><b>Ver detalhes</b></a>"&_
							MontaBotaoComprar(zero(rs("DisponivelParaCompra")), ProdutoID) &_ 
							"<a class=""details-quickview"" data-modal=""shadowbox;width=1000;height=520"" href="""&URL&"/quickview"" title=""Espiar"" rel=""nofollow""><b>Espiar</b></a>"&_
						"</div>"&_
					"</div>"&_
				"</li>"
			rs.MoveNext()
		Wend
		HTML = HTML & "</ul>"
		RetornarHTML = HTML		
	End Function
End Class

' =============================================
' .layout-2: Foto: Nome ao lado e Preço ao lado
' =============================================
Class Vitrine_Layout_2_Advogado
	Public Function RetornarHTML(rs)
		Dim HTML, ProdutoID, Url, NomeProduto, Preco, Descricao, Foto, Rating, strClass
		Set Flag = new Produtos_CamposEstendidos
		HTML = "<ul class=""products"">"
		While Not rs.EOF
			ProdutoID	= rs("ProdutoID")
			Url			= RetornarURLFriendly("p", rs("NomeDoProduto"), ProdutoID, "")
			NomeProduto	= HTMLEncode(rs("NomeDoProduto"))
			Preco		= MontaPrecoProduto(zero(rs("DisponivelParaCompra")), rs("Condicoes_Descricao"), rs("ProdutoID"), True, "") 'rs("Condicoes_Descricao")
			Descricao	= StripAllHTMLTags(Decodificar(rs("BreveDescricao")))
			If Len(Descricao) > 100 Then
				Descricao = Left(Descricao, 100) & " &hellip;"
			End If
			Foto		= GetFotoListagem(rs, 1)
			Foto		= IIF(Foto="", cfgHostImages(0)&"files/_fotos/semfoto.gif", Foto)
			Rating		= rs("Avaliacao")
			strClass	= ""
			If (Cstr(cfgControleDeEstoque)="1" And Not Cbool(zero(rs("DisponivelParaCompra")))) Then
				Preco	= "<a href=""popup_disponibilidade.asp?produtoid="&rs("ProdutoID")&""" data-modal=""shadowbox;width=300;height=360"" title=""Clique aqui para ser avisado quando este produto estiver dispon&iacute;vel"" class=""esgotado"">Esgotado</a>"
				strClass	=	"sold-out"
			End If

			ProdVisibilidade = True
			If rs("visualiza_preco") Then 
				If Session("Logado") <> "SIM" Then 				
					ProdVisibilidade = False
				End If 
			End If

			prevenda = ""
			If rs("prevenda") And rs("PreVendaData") >= Now Then
				prevenda = "<div class=""pre-ordable"">Pré-Venda até "&rs("PreVendaData")&"</div>"
			End If

			HasVariation = IIF(rs("HasVariation"),"1","0")
			HasSpecialAttribute = IIF(rs("HasSpecialAttribute"),"1","0")
			
			HTMLSold	=	"<div id=""products-sold-"&ProdutoID&""" class=""products-sold"">"&vbcrlf&_
								"<div class=""products-sold-header"">"&IIF(cfgExibirVendasProdutoLegend <> "",cfgExibirVendasProdutoLegend, "Vendidos")&"</div>"&vbcrlf&_
								"<span class=""products-sold-content"">"&rs("VendasEditavel")&"</span>"&vbcrlf&_
								"<div class=""products-sold-footer"">"&IIF(cfgExibirVendasProdutoFooter <> "",cfgExibirVendasProdutoFooter," ")&"</div>"&vbcrlf&_
							"</div>"
			
			strHTMLFLags = Flag.RenderizarFlags(rs,null,rs("produtoid"))
			
			HTML 	= 	HTML &_
						"<li class=""P"&ProdutoID&" "&strClass &""" id=""li-productid-"&ProdutoID&""">"&_
							"<input type=""hidden"" name=""ApresentacaoAlias"" value="""& rs("ApresentacaoAlias") &""">"&_
							"<input type=""hidden"" name=""HasVariation"" value="""& HasVariation &""">"&_
							"<input type=""hidden"" name=""HasSpecialAttribute"" value="""& HasSpecialAttribute &""">"&_
							"<div class=""product"">"&_
								"<a class=""photo"" href="""&Url&""" title="""&NomeProduto&"""><img class=""lazyload"" src=""/FILES/_IMG/spacer.gif"" data-src="""&Foto&""" data-gallery="""&rs("Foto")&""" title="""&NomeProduto&""" width="""&cfgLargura_Foto_Pequena&""" height="""&cfgAltura_Foto_Pequena&""" alt="""&NomeProduto&"""></a>"&_
								RenderVariationsList(rs, ProdutoID, ObjVariationsList, LastProductControl)&_
								"<div class=""product-info"">"&_
									"<div class=""product-name""><a href="""& Url &""" title="""&NomeProduto&""">"& rs("NomeDoProduto") &"</a></div>"&_
									"<div class=""product-autor"">"&rs("est_autor")&"</div>"&_
									"<span class=""rating rate-"&Rating&"""></span>"&_
									IIF(Descricao<>"", "<div class=""description""><a href="""&Url&""" title="""&Descricao&""">"&Descricao&"</a></div>", "")&_
									prevenda &_
									IIF(ProdVisibilidade,"<div class=""price"">"&Preco&"</div>","")&_
									IIF(rs("fretegratis"),"<span class=""label free-shipping"" title=""Frete Gr&aacute;tis""><b>Frete Gr&aacute;tis</b></span>","")&_
									IIF(rs("promocao") AND rs("promocaoinicio") < NOW AND rs("promocaofim") > NOW,"<span class=""label promotion"" title=""Promo&ccedil;&atilde;o""><b>Promo&ccedil;&atilde;o</b></span>","")&_
									strHTMLFLags&_
								"</div>"&_
								IIF(rs("ExibirVendasProduto"),HTMLSold,"")&vbcrlf&_
								"<div class=""buttons"">"&_
									"<a class=""details"" href="""&URL&""" title=""Ver detalhes""><b>Ver detalhes</b></a>"&_
									MontaBotaoComprar(zero(rs("DisponivelParaCompra")), ProdutoID) &_ 
									"<a class=""details-quickview"" data-modal=""shadowbox;width=1000;height=520"" href="""&URL&"/quickview"" title=""Espiar"" rel=""nofollow""><b>Espiar</b></a>"&_
								"</div>"&_
							"</div>"&_									
						"</li>"
			rs.MoveNext()
		Wend
		HTML = HTML & "</ul>"
		RetornarHTML = HTML
	End Function
End Class

' ===========================================================
' .layout-3: Foto: Nome ao lado e preço ao lado + Veja Também
' ===========================================================
Class Vitrine_Layout_3_Advogado
	Public Function RetornarHTML(rs)
		Dim HTML, ProdutoID, Url, NomeProduto, Preco, Descricao, Foto, Rating, PromocaoInicio, PromocaoFim, PrecoVejaTambem, strClass
		Set Flag = new Produtos_CamposEstendidos
		HTML =	"<ul class=""products"">"
		If Not rs.EOF Then
			ProdutoID	= rs("ProdutoID")
			Url			= RetornarURLFriendly("p", rs("NomeDoProduto"), ProdutoID, "")
			NomeProduto = HTMLEncode(rs("NomeDoProduto"))
			Preco		= MontaPrecoProduto(zero(rs("DisponivelParaCompra")), rs("Condicoes_Descricao"), rs("ProdutoID"), True, "") 'rs("Condicoes_Descricao")
			Descricao	= StripAllHTMLTags(Decodificar(rs("BreveDescricao")))
			If Len(Descricao) > 100 Then
				Descricao = Left(Descricao, 100) & " &hellip;"
			End If
			Foto		= GetFotoListagem(rs, 1)
			Foto		= IIF(Foto="", cfgHostImages(0)&"files/_fotos/semfoto.gif", Foto)
			Rating		= rs("Avaliacao")
			strClass	= ""
			If (Cstr(cfgControleDeEstoque)="1" And Not Cbool(zero(rs("DisponivelParaCompra")))) Then
				Preco	= "<a href=""popup_disponibilidade.asp?produtoid="&rs("ProdutoID")&""" data-modal=""shadowbox;width=300;height=360"" title=""Clique aqui para ser avisado quando este produto estiver dispon&iacute;vel"" class=""esgotado"">Esgotado</a>"
				strClass	=	"sold-out"
			End If

			ProdVisibilidade = True
			If rs("visualiza_preco") Then 
				If Session("Logado") <> "SIM" Then 				
					ProdVisibilidade = False
				End If 
			End If			

			prevenda = ""
			If rs("prevenda") And rs("PreVendaData") >= Now Then
				prevenda = "<div class=""pre-ordable"">Pré-Venda até "&rs("PreVendaData")&"</div>"
			End If

			HasVariation = IIF(rs("HasVariation"),"1","0")
			HasSpecialAttribute = IIF(rs("HasSpecialAttribute"),"1","0")
			
			HTMLSold	=	"<div id=""products-sold-"&ProdutoID&""" class=""products-sold"">"&vbcrlf&_
								"<div class=""products-sold-header"">"&IIF(cfgExibirVendasProdutoLegend <> "",cfgExibirVendasProdutoLegend, "Vendidos")&"</div>"&vbcrlf&_
								"<span class=""products-sold-content"">"&rs("VendasEditavel")&"</span>"&vbcrlf&_
								"<div class=""products-sold-footer"">"&IIF(cfgExibirVendasProdutoFooter <> "",cfgExibirVendasProdutoFooter," ")&"</div>"&vbcrlf&_
							"</div>"

			strHTMLFLags = Flag.RenderizarFlags(rs,null,rs("produtoid"))

			HTML	=	HTML &_
						"<li class=""P"&ProdutoID&" "&strClass &""" id=""li-productid-"&ProdutoID&""">"&_
							"<input type=""hidden"" name=""ApresentacaoAlias"" value="""& rs("ApresentacaoAlias") &""">"&_
							"<input type=""hidden"" name=""HasVariation"" value="""& HasVariation &""">"&_
							"<input type=""hidden"" name=""HasSpecialAttribute"" value="""& HasSpecialAttribute &""">"&_
							"<div class=""product"">"&_
								"<a href="""&Url&""" class=""photo"" title="""&NomeProduto&"""><img class=""lazyload"" src=""/FILES/_IMG/spacer.gif"" data-src="""&Foto&""" data-gallery="""&rs("Foto")&""" width="""&cfgLargura_Foto_Pequena&""" height="""&cfgAltura_Foto_Pequena&""" alt="""&NomeProduto&""" title="""&NomeProduto&"""></a>"&_
								RenderVariationsList(rs, ProdutoID, ObjVariationsList, LastProductControl)&_
								"<div class=""product-info"">"&_
									"<div class=""product-name""><a href="""&Url&""" title="""&NomeProduto&""">"&NomeProduto&"</a></div>"&_
									"<div class=""product-autor"">"&rs("est_autor")&"</div>"&_
									"<span class=""rating rate-"&Rating&"""></span>"&_
									IIF(Descricao<>"", "<div class=""description""><a href="""&Url&""" title="""&Descricao&""">"&Descricao&"</a></div>", "")&_									
									prevenda &_
									IIF(ProdVisibilidade,"<div class=""price"">"&Preco&"</div>","")&_
									IIF(rs("fretegratis"),"<span class=""label free-shipping"" title=""Frete Gr&aacute;tis""><b>Frete Gr&aacute;tis</b></span>","")&_
									IIF(rs("promocao") AND rs("promocaoinicio") < NOW AND rs("promocaofim") > NOW,"<span class=""label promotion"" title=""Promo&ccedil;&atilde;o""><b>Promo&ccedil;&atilde;o</b></span>","")&_
									strHTMLFLags&_
								"</div>"&_
								IIF(rs("ExibirVendasProduto"),HTMLSold,"")&vbcrlf&_
								"<div class=""buttons"">"&_
									"<a class=""details"" href="""& URL &""" title=""Ver detalhes""><b>Ver detalhes</b></a>"&_
									MontaBotaoComprar(zero(rs("DisponivelParaCompra")), ProdutoID) & _
									"<a class=""details-quickview"" data-modal=""shadowbox;width=1000;height=520"" href="""&URL&"/quickview"" title=""Espiar"" rel=""nofollow""><b>Espiar</b></a>"&_
								"</div>"&_
							"</div>"&_
						"</li>"&_
						"</ul>"
			rs.MoveNext()
			If Not rs.EOF Then
				HTML = HTML &_
						"<span class=""see-also-title"">Veja também</span>" &_
						"<ul class=""see-also products"">"
				While Not rs.EOF
					ProdutoID			= rs("ProdutoID")
					Url					= RetornarURLFriendly("p", rs("NomeDoProduto"), ProdutoID, "")
					NomeProduto			= HTMLEncode(rs("NomeDoProduto"))
					PromocaoInicio		= IIF(IsDate(rs("promocaoinicio")), rs("promocaoinicio"), Date())
					PromocaoFim			= IIF(IsDate(rs("promocaofim")), rs("promocaofim"), Date())	
					PrecoVejaTambem		= FormatCurrency( IIF( rs("promocao") And PromocaoInicio <= Date() And PromocaoFim >= Date() And Zero(rs("precopromocao")) > 0, Zero(rs("precopromocao")), Zero(rs("preco")) ) )
					If (Cstr(cfgControleDeEstoque)="1" And Not Cbool(zero(rs("DisponivelParaCompra")))) Then
						PrecoVejaTambem	= "<a href=""popup_disponibilidade.asp?produtoid="&rs("ProdutoID")&""" data-modal=""shadowbox;width=300;height=360"" title=""Clique aqui para ser avisado quando este produto estiver dispon&iacute;vel"">Esgotado</a>"
					End If

					HTML 							= HTML &_
						"<li><b class=""product-name""><a href="""& Url &""" title="""& NomeProduto &""">"& NomeProduto &"</a></b> &middot; <span class=""price""><b class=""sale"">"& PrecoVejaTambem &"</b></span></li>"
					rs.MoveNext()
				Wend
				HTML = HTML & "</ul>"
			End If
		
		End If
		RetornarHTML = HTML		
	End Function
End Class

' ====================================================
' .layout-4: Foto Grande: Nome ao lado e Preço ao lado
' ====================================================
Class Vitrine_Layout_4_Advogado
	Public Function RetornarHTML(rs)
		Dim HTML, ProdutoID, Url, NomeProduto, Preco, Descricao, Rating, Foto, strClass
		Set Flag = new Produtos_CamposEstendidos
		HTML = "<ul class=""products"">"
		While Not rs.EOF
			ProdutoID	= rs("ProdutoID")
			Url			= RetornarURLFriendly("p", rs("NomeDoProduto"), ProdutoID, "")
			NomeProduto	= HTMLEncode(rs("NomeDoProduto"))
			Preco		= MontaPrecoProduto(zero(rs("DisponivelParaCompra")), rs("Condicoes_Descricao"), rs("ProdutoID"), True, "") 'rs("Condicoes_Descricao")
			Descricao	= StripAllHTMLTags(Decodificar(rs("BreveDescricao")))
			If Len(Descricao) > 100 Then
				Descricao = Left(Descricao, 100) & " &hellip;"
			End If
			Rating 		= rs("Avaliacao")
			strClass	= ""
			If (Cstr(cfgControleDeEstoque)="1" And Not Cbool(zero(rs("DisponivelParaCompra")))) Then
				Preco	= "<a href=""popup_disponibilidade.asp?produtoid="&rs("ProdutoID")&""" data-modal=""shadowbox;width=300;height=360"" title=""Clique aqui para ser avisado quando este produto estiver dispon&iacute;vel"" class=""esgotado"">Esgotado</a>"
				strClass	=	"sold-out"
			End If
			Foto		= GetFotoDetalhe(rs, 1)
			Foto		= IIF(Foto="", cfgHostImages(0)&"files/_fotos/semfoto.gif", Foto)

			ProdVisibilidade = True
			If rs("visualiza_preco") Then 
				If Session("Logado") <> "SIM" Then 				
					ProdVisibilidade = False
				End If 
			End If			

			prevenda = ""
			If rs("prevenda") And rs("PreVendaData") >= Now Then
				prevenda = "<div class=""pre-ordable"">Pré-Venda até "&rs("PreVendaData")&"</div>"
			End If

			HasVariation = IIF(rs("HasVariation"),"1","0")
			HasSpecialAttribute = IIF(rs("HasSpecialAttribute"),"1","0")
			
			HTMLSold	=	"<div id=""products-sold-"&ProdutoID&""" class=""products-sold"">"&vbcrlf&_
								"<div class=""products-sold-header"">"&IIF(cfgExibirVendasProdutoLegend <> "",cfgExibirVendasProdutoLegend, "Vendidos")&"</div>"&vbcrlf&_
								"<span class=""products-sold-content"">"&rs("VendasEditavel")&"</span>"&vbcrlf&_
								"<div class=""products-sold-footer"">"&IIF(cfgExibirVendasProdutoFooter <> "",cfgExibirVendasProdutoFooter," ")&"</div>"&vbcrlf&_
							"</div>"

			strHTMLFLags = Flag.RenderizarFlags(rs,null,rs("produtoid"))

			HTML	=	HTML &_
						"<li class=""P"&ProdutoID&" "&strClass &""" id=""li-productid-"&ProdutoID&""">"&_
							"<input type=""hidden"" name=""ApresentacaoAlias"" value="""& rs("ApresentacaoAlias") &""">"&_
							"<input type=""hidden"" name=""HasVariation"" value="""& HasVariation &""">"&_
							"<input type=""hidden"" name=""HasSpecialAttribute"" value="""& HasSpecialAttribute &""">"&_
							"<div class=""product"">"&_
								"<a href="""&Url&""" class=""photo"" title="""&NomeProduto&"""><img class=""lazyload"" src=""/FILES/_IMG/spacer.gif"" data-src="""&Foto&""" data-gallery="""&rs("Foto")&""" alt="""&NomeProduto&""" title="""&NomeProduto&"""></a>"&_
								RenderVariationsList(rs, ProdutoID, ObjVariationsList, LastProductControl)&_
								"<div class=""product-info"">"&_
									"<div class=""product-name""><a href="""&Url&""" title="""&NomeProduto&""">"&NomeProduto&"</a></div>"&_
									"<div class=""product-autor"">"&rs("est_autor")&"</div>"&_
									"<span class=""rating rate-"&Rating&"""></span>"&_
									IIF(Descricao<>"", "<div class=""description""><a href="""&Url&""" title="""&Descricao&""">"&Descricao&"</a></div>", "")&_
									prevenda &_
									IIF(ProdVisibilidade,"<div class=""price"">"&Preco&"</div>","")&_
									IIF(rs("fretegratis"),"<span class=""label free-shipping"" title=""Frete Gr&aacute;tis""><b>Frete Gr&aacute;tis</b></span>","")&_
									IIF(rs("promocao") AND rs("promocaoinicio") < NOW AND rs("promocaofim") > NOW,"<span class=""label promotion"" title=""Promo&ccedil;&atilde;o""><b>Promo&ccedil;&atilde;o</b></span>","")&_
									strHTMLFLags&_
									IIF(rs("ExibirVendasProduto"),HTMLSold,"")&vbcrlf&_
									"<div class=""buttons"">"&_
										"<a class=""details"" href="""&URL&""" title=""Ver detalhes""><b>Ver detalhes</b></a>"&_
										MontaBotaoComprar(zero(rs("DisponivelParaCompra")), ProdutoID) &_ 
										"<a class=""details-quickview"" data-modal=""shadowbox;width=1000;height=520"" href="""&URL&"/quickview"" title=""Espiar"" rel=""nofollow""><b>Espiar</b></a>"&_
									"</div>"&_
								"</div>"&_
							"</div>"&_
						"</li>"
			rs.MoveNext()
		Wend
		HTML			= HTML & "</ul>"
		RetornarHTML	= HTML		
	End Function
End Class

' ================================
' .layout-5: Carrousel de Produtos
' ================================
Class Vitrine_Layout_5_Advogado
	Public Function RetornarHTML(rs)
		Dim HTML, ProdutoID, Url, NomeProduto, Foto, Preco, Rating, strClass
		Set Flag = new Produtos_CamposEstendidos
		HTML = "<ul class=""products carousel"">"
		While Not rs.EOF
			ProdutoID	= rs("ProdutoID")
			Url			= RetornarURLFriendly("p", rs("NomeDoProduto"), ProdutoID, "")
			NomeProduto	= HTMLEncode(rs("NomeDoProduto"))
			Foto		= GetFotoListagem(rs, 1)
			Foto		= IIF(Foto="", cfgHostImages(0)&"files/_fotos/semfoto.gif", Foto)
			Preco		= MontaPrecoProduto(zero(rs("DisponivelParaCompra")), rs("Condicoes_Descricao"), rs("ProdutoID"), True, "") 'rs("Condicoes_Descricao")
			Rating 		= rs("Avaliacao")
			strClass	= ""
			If (Cstr(cfgControleDeEstoque)="1" And Not Cbool(zero(rs("DisponivelParaCompra")))) Then
				Preco	= "<a href=""popup_disponibilidade.asp?produtoid="&rs("ProdutoID")&""" data-modal=""shadowbox;width=300;height=360"" title=""Clique aqui para ser avisado quando este produto estiver dispon&iacute;vel"" class=""esgotado"">Esgotado</a>"
				strClass	=	"sold-out"
			End If

			ProdVisibilidade = True
			If rs("visualiza_preco") Then 
				If Session("Logado") <> "SIM" Then 				
					ProdVisibilidade = False
				End If 
			End If			

			prevenda = ""
			If rs("prevenda") And rs("PreVendaData") >= Now Then
				prevenda = "<div class=""pre-ordable"">Pré-Venda até "&rs("PreVendaData")&"</div>"
			End If

			HasVariation = IIF(rs("HasVariation"),"1","0")
			HasSpecialAttribute = IIF(rs("HasSpecialAttribute"),"1","0")
			
			HTMLSold	=	"<div id=""products-sold-"&ProdutoID&""" class=""products-sold"">"&vbcrlf&_
								"<div class=""products-sold-header"">"&IIF(cfgExibirVendasProdutoLegend <> "",cfgExibirVendasProdutoLegend, "Vendidos")&"</div>"&vbcrlf&_
								"<span class=""products-sold-content"">"&rs("VendasEditavel")&"</span>"&vbcrlf&_
								"<div class=""products-sold-footer"">"&IIF(cfgExibirVendasProdutoFooter <> "",cfgExibirVendasProdutoFooter," ")&"</div>"&vbcrlf&_
							"</div>"
			
			strHTMLFLags = Flag.RenderizarFlags(rs,null,rs("produtoid"))

			HTML	=	HTML&_
						"<li class=""P"&ProdutoID&" "&strClass &""" id=""li-productid-"&ProdutoID&""">"&_
							"<input type=""hidden"" name=""ApresentacaoAlias"" value="""& rs("ApresentacaoAlias") &""">"&_
							"<input type=""hidden"" name=""HasVariation"" value="""& HasVariation &""">"&_
							"<input type=""hidden"" name=""HasSpecialAttribute"" value="""& HasSpecialAttribute &""">"&_
							"<div class=""product"">"&_
								"<div class=""product-name""><a href="""& Url &""" title="""& NomeProduto &""">"& NomeProduto &"</a></div>"&_
								"<div class=""product-autor"">"&rs("est_autor")&"</div>"&_
								"<a class=""photo"" href="""& Url &""" title="""& NomeProduto &"""><img class=""lazyload"" src=""/FILES/_IMG/spacer.gif"" data-src="""&Foto&""" data-gallery="""&rs("Foto")&""" width="""& cfgLargura_Foto_Pequena &""" height="""& cfgAltura_Foto_Pequena &""" alt="""& NomeProduto &""" title="""& NomeProduto &"""></a>"&_
								RenderVariationsList(rs, ProdutoID, ObjVariationsList, LastProductControl)&_
								"<span class=""rating rate-"&Rating&"""></span>"&_
								prevenda &_
								IIF(ProdVisibilidade,"<div class=""price"">"&Preco&"</div>","")&_
								IIF(rs("fretegratis"),"<span class=""label free-shipping"" title=""Frete Gr&aacute;tis""><b>Frete Gr&aacute;tis</b></span>","")&_
								IIF(rs("promocao") AND rs("promocaoinicio") < NOW AND rs("promocaofim") > NOW,"<span class=""label promotion"" title=""Promo&ccedil;&atilde;o""><b>Promo&ccedil;&atilde;o</b></span>","")&_
								strHTMLFLags&_		
								IIF(rs("ExibirVendasProduto"),HTMLSold,"")&vbcrlf&_
								"<div class=""buttons"">"&_
									"<a class=""details"" href="""&URL&""" title=""Ver detalhes""><b>Ver detalhes</b></a>"&_
									MontaBotaoComprar(zero(rs("DisponivelParaCompra")), ProdutoID) &_ 
									"<a class=""details-quickview"" data-modal=""shadowbox;width=1000;height=520"" href="""&URL&"/quickview"" title=""Espiar"" rel=""nofollow""><b>Espiar</b></a>"&_
								"</div>"&_
							"</div>"&_
						"</li>"
			rs.MoveNext()
		Wend
		HTML 			= HTML & "</ul>"
		RetornarHTML 	= HTML		
	End Function
End Class

' ================================
' .layout-5: Carrousel 3D de produtos
' ================================
Class Vitrine_Layout_6_Advogado
	Public Function RetornarHTML(rs)

		Dim HTML, ProdutoID, Url, NomeProduto, Foto, Preco, Rating
		HTML = "<div id=""carousel3D"" style=""width: 100%; height: 400px; overflow: scroll;"">"

		While Not rs.EOF
			ProdutoID	= rs("ProdutoID")
			Url			= RetornarURLFriendly("p", rs("NomeDoProduto"), ProdutoID, "")
			NomeProduto	= HTMLEncode(rs("NomeDoProduto"))
			Foto		= GetFotoListagem(rs, 1)
			Foto		= IIF(Foto="", cfgHostImages(0)&"files/_fotos/semfoto.gif", Foto)
			'Preco		= MontaPrecoProduto(zero(rs("DisponivelParaCompra")), FormatCurrency(rs("Preco")), rs("ProdutoID"), False, "") 'FormatCurrency(rs("Preco"))
			Preco		= "<span class=""price"">"&MontaPrecoProduto(zero(rs("DisponivelParaCompra")), rs("Condicoes_Descricao"), rs("ProdutoID"), True, "")&"</span>"
			
			ProdVisibilidade = True
			If rs("visualiza_preco") Then 
				If Session("Logado") <> "SIM" Then 				
					ProdVisibilidade = False
				End If 
			End If

			HTML	=	HTML & "<a href=""" & Url & """><img class=""cloudcarousel lazyload"" src=""/FILES/_IMG/spacer.gif"" data-src="""&Foto & """ alt="""& HTMLEncode(Preco) &""" title="""& NomeProduto &""" /></a>"
			rs.MoveNext()
		Wend

		RetornarHTML = HTML & "<input id=""left-but"" type=""button"" value=""Left"" />" &_
					   "<input id=""right-but"" type=""button"" value=""Right"" />" &_
					   	"<div class=""cloudcarousel-infos"">"&_
							"<div id=""title-text"">nome</div>" &_
					  	 	"<div id=""alt-text"">preco</div>" &_
						"</div>" &_
					   "</div>"
	End Function
End Class

%>