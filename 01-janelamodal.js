﻿function cadastraNews(form){
    var form = $(form);
    var email = form.find('[name=email]').val()||'';    
    if(!~email.indexOf('@') || !~email.indexOf('.')){
        showMessage('Digite um e-mail válido, por favor.', 'Oppss... :(');
            $('#system-message .message-wrapper .close-message').css('width','120px');
        return false;
    }
    // if(!$('.inputs-form input').is(':checked').length){
    //     showMessage('Selecione ao menos uma opção', 'Oppss... :(');
    //     return false;
    // }
    $.ajax({
        type: 'POST',
        url: form.attr('action'),
        data: form.serialize(),
        cache: false,
        success: function(){
            showMessage('Obrigado! Em instantes você receberá um e-mail com seu cupom de desconto!"', 'Parabéns!');
            $('#system-message .message-wrapper .close-message b').text('Ir a Loja');            
            $('#interContainer, #interVeil').hide();                
        }, 
        error: function(){
            showMessage('Ocorreu um erro ao cadastrar seu e-mail.', 'Oppss... :(');
        }
    });
    return false;
}



//Interstitial Content Box v1.1- http://www.dynamicdrive.com/dynamicindex17/interstitial.htm
//Last modified: Nov 26th, 06' (New: disable webpage scrollbar, auto hide after x seconds options, 
/*
    Here's an explanation of each of these variables:

    Syntax: ["samplecontent"] OR ["samplecontent", "samplecontent2", "etc"] The file(s) on your server to fetch and display inside the interstitial box. If more than one is specified, the script will randomly pick one to display.

    The display frequency of the interstitial box. Three types of frequency are supported- probability, once per browser session, and once every x days.

        Probability Syntax: ["chance", "3"] This setting causes the box to be shown randomly based on a probability of 1/x. The "3" in this case means there's a 1/3 chance the box is shown each time the page loads. "1" would mean 100% of the time. Change it to any desired integer.

        Once per browser session Syntax: ["cookie", "session"] This setting causes the box to pop up once per browser session. A new session begins when the visitor has closed his browser and relaunches it.

        Once every x days Syntax: ["cookie", "5"] This setting causes the box to pop up once every "x" days, where "5" in this case would mean once every 5 days.

    The HTML that make up the title bar of the interstitial box interface. You can customize it as desired.

    Syntax: ["stitialcookie", "path=/"] If the display frequency is set to "once per browser session" or "x days" above, both of which relies on cookies, you can fine tune the cookie settings here. The first value is the arbitrary cookie name to use, and the later, the path of the cookie.

    Syntax: true/false Configures whether the script should stop potential caching of the fetched external page when it is displayed more than once to the same viewer. Setting it to "true" for example (bust cache) could be helpful if your external page contains an advertisement, and you wish each request on the ad to be unique instead of potentially cached.

    Syntax: true/false Specifies whether the document's scrollbars should be disabled while the Interstitial is being shown. This ensures that the user's focus is automatically set on the interstitial (instead of in some cases having to scroll the document). Note that this feature is only functional in IE7+, Firefox 1.x+, and Opera 8+. In IE6 and below, a CSS bug means it is opted out of this feature.

    Syntax: An integer (0 or above). Sets whether the Interstitial Box should automatically disappear after x seconds (0 disables it). Any integer value greater than 0 sets this feature in motion, so 4 for example would cause the box to auto close after 4 seconds.

*/

var interstitialBox={
    //1) Lista de arquivos no servidor para escolher aleatoriamente e exibir
    //arquivos para exibir: ['samplecontent.htm', 'samplecontent2.htm'],
    displayfiles: ['/files/_arquivos/janelamodal/janelamodal.htm'],

//2) frequência de exibição: ["frequency_type", "frequency_value"]
displayfrequency: ["cookie", "session"],

//3) HTML para o cabeçalho da barra da caixa
defineheader: '<div class="headerbar"><a href="#" onClick="javascript:interstitialBox.closeit(); return false"></a></div>',

//4) cookie setting: ["cookie_name", "cookie_path"]
cookiesetting: ["stitialcookie", "path=/"],

//5) Busto de cache das páginas buscadas através de Ajax??
ajaxbustcache: true,

//6) Desabilita a barra de rolagem enquanto a modal é exibida (Apenas aplicável no IE7/Firefox/Opera8+. IE6 irá apenas autorolar para cima)?
disablescrollbars: false,

//7) Oculta a janela após x segundos (0 para não)?
autohidetimer: 0,

////Não é necessário editar para além daqui//////////////////////////////////

ie7: window.XMLHttpRequest && document.all && !window.opera,
ie7offline: this.ie7 && window.location.href.indexOf("http")==-1, //check for IE7 and offline
launch:false,
scrollbarwidth: 16,

ajaxconnect:function(url, thediv){
var page_request = false
var bustcacheparameter=""
if (window.XMLHttpRequest && !this.ie7offline) // Se Mozilla, IE7 online, Safari etc
page_request = new XMLHttpRequest()
else if (window.ActiveXObject) { // Se IE6 ou abaixo, ou IE7 off-line (para fins de teste)
try {
page_request = new ActiveXObject("Msxml2.XMLHTTP")
} 
catch (e){
try{
page_request = new ActiveXObject("Microsoft.XMLHTTP")
}
catch (e){}
}
}
else
return false
page_request.onreadystatechange=function(){
interstitialBox.loadpage(page_request, thediv)
}
if (this.ajaxbustcache) //if bust caching of external page
bustcacheparameter=(url.indexOf("?")!=-1)? "&"+new Date().getTime() : "?"+new Date().getTime()
page_request.open('GET', url+bustcacheparameter, true)
page_request.send(null)
},

loadpage:function(page_request, thediv){
if (page_request.readyState == 4 && (page_request.status==200 || window.location.href.indexOf("http")==-1)){
document.getElementById("interContent").innerHTML=page_request.responseText
}
},

createcontainer:function(){
//write out entire HTML for Interstitial Box:
document.write('<div id="interContainer">'+this.defineheader+'<div id="interContent"></div></div><div id="interVeil"></div>')
this.interContainer=document.getElementById("interContainer") //reference interstitial container
this.interVeil=document.getElementById("interVeil") //reference veil
this.standardbody=(document.compatMode=="CSS1Compat")? document.documentElement : document.body //create reference to common "body" across doctypes
},


showcontainer:function(){
if (this.interContainer.style.display=="none") return //if interstitial box has already closed, just exit (window.onresize event triggers function)
var ie=document.all && !window.opera
var dom=document.getElementById
var scroll_top=(ie)? this.standardbody.scrollTop : window.pageYOffset
var scroll_left=(ie)? this.standardbody.scrollLeft : window.pageXOffset
var docwidth=(ie)? this.standardbody.clientWidth : window.innerWidth-this.scrollbarwidth
var docheight=(ie)? this.standardbody.clientHeight: window.innerHeight
var docheightcomplete=(this.standardbody.offsetHeight>this.standardbody.scrollHeight)? this.standardbody.offsetHeight : this.standardbody.scrollHeight
var objwidth=this.interContainer.offsetWidth
var objheight=this.interContainer.offsetHeight
this.interVeil.style.width=docwidth+"px" //set up veil over page
this.interVeil.style.height=docheightcomplete+"px" //set up veil over page
this.interVeil.style.left=0 //Position veil over page
this.interVeil.style.top=0 //Position veil over page
this.interVeil.style.visibility="visible" //Show veil over page
this.interContainer.style.left=docwidth/2-objwidth/2+"px" //Position interstitial box
var topposition=(docheight>objheight)? scroll_top+docheight/2-objheight/2+"px" : scroll_top+5+"px" //Position interstitial box
this.interContainer.style.top=Math.floor(parseInt(topposition))+"px"
this.interContainer.style.visibility="visible" //Show interstitial box
if (this.autohidetimer && parseInt(this.autohidetimer)>0 && typeof this.timervar=="undefined")
this.timervar=setTimeout("interstitialBox.closeit()", this.autohidetimer*1000)
},


closeit:function(){
this.interVeil.style.display="none"
this.interContainer.style.display="none"
if (this.disablescrollbars && window.XMLHttpRequest) //if disablescrollbars enabled and modern browsers- IE7, Firefox, Safari, Opera 8+ etc
this.standardbody.style.overflow="auto"
if (typeof this.timervar!="undefined") clearTimeout(this.timervar)
},

getscrollbarwidth:function(){
var scrollbarwidth=window.innerWidth-(this.interVeil.offsetLeft+this.interVeil.offsetWidth) //http://www.howtocreate.co.uk/emails/BrynDyment.html
this.scrollbarwidth=(typeof scrollbarwidth=="number")? scrollbarwidth : this.scrollbarwidth
},

hidescrollbar:function(){
if (this.disablescrollbars){ //if disablescrollbars enabled
if (window.XMLHttpRequest) //if modern browsers - IE7, Firefox, Safari, Opera 8+ etc
this.standardbody.style.overflow="hidden"
else //if IE6 and below, just scroll to top of page to ensure interstitial is in focus
window.scrollTo(0,0)
}
},

dotask:function(target, functionref, tasktype){ //assign a function to execute to an event handler (ie: onunload)
var tasktype=(window.addEventListener)? tasktype : "on"+tasktype
if (target.addEventListener)
target.addEventListener(tasktype, functionref, false)
else if (target.attachEvent)
target.attachEvent(tasktype, functionref)
},

initialize:function(){
this.createcontainer() //write out interstitial container
this.ajaxconnect(this.displayfiles[Math.floor(Math.random()*this.displayfiles.length)], this.interContainer) //load page into content via ajax
this.dotask(window, function(){interstitialBox.hidescrollbar(); interstitialBox.getscrollbarwidth(); setTimeout("interstitialBox.showcontainer()", 100)}, "load")
this.dotask(window, function(){interstitialBox.showcontainer()}, "resize")
}
}

/////////////End of interstitialBox object declaration here ////////////////////////////////

function getCookie(Name){
var re=new RegExp(Name+"=[^;]+", "i"); //construct RE to search for target name/value pair
if (document.cookie.match(re)) //if cookie found
return document.cookie.match(re)[0].split("=")[1] //return its value
return null
}

function setCookie(name, value, days){
var expireDate = new Date()
//set "expstring" to either an explicit date (past or future)
if (typeof days!="undefined"){ //if set persistent cookie
var expstring=expireDate.setDate(expireDate.getDate()+parseInt(days))
document.cookie = name+"="+value+"; expires="+expireDate.toGMTString()+"; "+interstitialBox.cookiesetting[1]
}
else //else if this is a session only cookie setting
document.cookie = name+"="+value+"; "+interstitialBox.cookiesetting[1]
}


var stitialvars=new Object() //temporary object to reference/ shorthand certain interstitialBox properties
stitialvars.freqtype=interstitialBox.displayfrequency[0] //"chance" or "cookie"
stitialvars.cookieduration=interstitialBox.displayfrequency[1] //"session" or int (integer specifying number of days)
stitialvars.cookiename=interstitialBox.cookiesetting[0] //name of cookie to use


if (stitialvars.freqtype=="chance"){ //IF CHANCE MODE
if (Math.floor(Math.random()*interstitialBox.displayfrequency[1])==0)
interstitialBox.launch=true
}
else if (stitialvars.freqtype=="cookie" && stitialvars.cookieduration=="session"){ //IF "SESSION COOKIE" MODE
if (getCookie(stitialvars.cookiename+"_s")==null){ //if session cookie is empty
setCookie(stitialvars.cookiename+"_s", "loaded")
interstitialBox.launch=true
}
}
else if (stitialvars.freqtype=="cookie" && typeof parseInt(stitialvars.cookieduration)=="number"){ //IF "PERSISTENT COOKIE" MODE
if (getCookie(stitialvars.cookiename)==null || parseInt(getCookie(stitialvars.cookiename))!=parseInt(stitialvars.cookieduration)){ //if persistent cookie is empty or admin has changed number of days to persist from that of the stored value (meaning, reset it)
setCookie(stitialvars.cookiename, stitialvars.cookieduration, stitialvars.cookieduration)
interstitialBox.launch=true
} 
}

if (interstitialBox.launch){
    interstitialBox.initialize();
}