jQuery.fn.css2 = jQuery.fn.css;
jQuery.fn.css = function() {
    if (arguments.length){return jQuery.fn.css2.apply(this, arguments);}
    var attr = ['font-family','font-size','font-weight','font-style','color',
        'text-transform','text-decoration','letter-spacing','word-spacing',
        'line-height','text-align','vertical-align','direction','background-color',
        'background-image','background-repeat','background-position',
        'background-attachment','opacity','width','height','top','right','bottom',
        'left','margin-top','margin-right','margin-bottom','margin-left',
        'padding-top','padding-right','padding-bottom','padding-left',
        'border-top-width','border-right-width','border-bottom-width',
        'border-left-width','border-top-color','border-right-color',
        'border-bottom-color','border-left-color','border-top-style',
        'border-right-style','border-bottom-style','border-left-style','position',
        'display','visibility','z-index','overflow-x','overflow-y','white-space',
        'clip','float','clear','cursor','list-style-image','list-style-position',
        'list-style-type','marker-offset'];
    var len = attr.length, obj = {};
    for (var i = 0; i < len; i++) {
        obj[attr[i]] = jQuery.fn.css2.call(this, attr[i]);
	}
    return obj;
};
jQuery(function(){
	if($('#btn-buy:visible').length>0 && $('#form-kit').length==0){
		var htmlPreco = '';
		var price = $('.purchase-info .price').html();
		if(typeof(price) === 'undefined'){
			price = 0;
			var kit = $('#product-kit');
			if(kit.length){
				var varsMgn = $('.grid-price-variation .entry-variation', kit);
				varsMgn.each(function(){
					var tmpPrice = $('.price .savings b:first', this);
					if (typeof(tmpPrice) !== 'undefined'){
						tmpPrice = parseFloat(tmpPrice.html().replace('R$','').replace('.','').trim().replace(',','.'));
						if(tmpPrice < price || price == 0){
							price = tmpPrice;
							priceToExhibition = $('.price',this).html();
						}
					}
				});
			}
			if(price > 0){
				htmlPreco = '<div class="title">Preço do produto principal</div><div class="price">'+priceToExhibition +'</div>';
			}
		}else {
			htmlPreco = '<div class="title">Preço do produto principal</div><div class="price">'+price +'</div>';
		}
		$('body').append('<div class="magnetic-purchase">'+htmlPreco+'<button>Comprar</button></div>');
		$('.magnetic-purchase button').css($('#btn-buy:visible').css()).css({'position':'static','text-indent':'-9999em'}).click(
			function(){
				if(! $('form[name="buy"]').submit().valid() ){
					topElem = $('.input-error-global').first().offset().top;
					if( !(topElem > $(window).scrollTop() && topElem < ($(window).scrollTop() + $(window).height())) ){
					var topError = $('.input-error-global').first().offset().top - 120;	
		 			$('html, body').animate({
		 				scrollTop:topError 
		 			},350);
		 		}
			}
		});
		$('.magnetic-purchase').css('border-color',$('.purchase-info').css('background-color'));
	
		$(window).bind('scroll',function(){
			var win = $(window);
			var el = $('#btn-buy');
			var winPos = win.scrollTop();
			var elPos = el.offset().top + el.height();		
			if( winPos > elPos ){
				$('.magnetic-purchase').fadeIn();
			}else{
				$('.magnetic-purchase').fadeOut();
			}
		});
	}
});