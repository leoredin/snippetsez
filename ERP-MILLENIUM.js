"use strict";

const lodashScript = document.createElement("script");
lodashScript.src = "https://cdn.jsdelivr.net/npm/lodash@4.17.10/lodash.min.js";
$("body").append(lodashScript);

const warehouseId = 1;
const inventoryMovementTypes = 2;
const actionEnum = "2";
let jsonValues = [...this.json.value];

Object.keys(jsonValues).forEach(function(key) {
    jsonValues[key].WarehouseID = warehouseId;
    jsonValues[key].InventoryMovementTypes = inventoryMovementTypes;
    jsonValues[key].ActionEnum = actionEnum;
});

let jsonAjustado = _.map(jsonValues, function(value) {
    return _.pick(
        value,
        "sku",
        "id",
        "saldo",
        "WarehouseID",
        "InventoryMovementTypes",
        "ActionEnum"
    );
});

jsonAjustado = JSON.stringify(jsonAjustado);
jsonAjustado = jsonAjustado.replace(/\"sku\":/g, '"SKU":');
jsonAjustado = jsonAjustado.replace(/\"id\":/g, '"integrationID":');
jsonAjustado = jsonAjustado.replace(/\"saldo\":/g, '"Quantity":');

jsonAjustado = JSON.parse(jsonAjustado);

Object.keys(jsonValues).forEach(function(key) {
    $.ajax({
        url: "/v1/Inventory/API.svc/web/ChangeSKUInventory",
        type: "post",
        data: jsonAjustado[key],
        beforeSend: function(xhr) {
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("Accept", "application/json");
        },
        success: function(r) {
            console.log(eval(r));
        }
    });
});
