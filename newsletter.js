$(document).ready(() => {
  $("#newsletter form a.submit").on("click", function() {
    const validator = $(this).validate();
    const errors = validator.numberOfInvalids();
    if (errors == 0) {
      const email = $("#newsletter-email").val(),
        name = $("#newsletter-name").val();
      $.ajax({
        url: "https://news.mailclick.me/subscribe.php",
        type: "post",
        data: {
          "FormValue_Fields[EmailAddress]": email,
          "FormValue_Fields[CustomField4185]": name,
          "FormValue_ListID": 4310,
          "FormValue_Command": "Subscriber.Add",
          "FormButton_Subscribe": "OK"
        },
        success: function(res) {
          console.log("mlbz ok");
        }
      });
    }
  });
});
