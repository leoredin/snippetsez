if($('#page-produtos').length){
    $(".product-description.component").after($("<div class='campos-extendidos'></div>"));
    //autor
    if(extendedFields.exest_autor) $(".campos-extendidos").append('<p><b>Autor</b> : ' + extendedFields.exest_autor + '</p>');
    //edição
    if(extendedFields.exest_edicao) $(".campos-extendidos").append('<p><b>Edição</b> : ' + extendedFields.exest_edicao + '</p>');
    //Ano
    if(extendedFields.exest_ano) $(".campos-extendidos").append('<p><b>Ano</b> : ' + extendedFields.exest_ano + '</p>');
    //isbn
    if(extendedFields.exest_isbn) $(".campos-extendidos").append('<p><b>ISBN</b> : ' + extendedFields.exest_isbn + '</p>');
    //páginas
    if(extendedFields.exest_paginas) $(".campos-extendidos").append('<p><b>Páginas</b> : ' + extendedFields.exest_paginas + '</p>');
    //formato
    if(extendedFields.exest_formato) $(".campos-extendidos").append('<p><b>Formato</b> : ' + extendedFields.exest_formato + '</p>');
    //acabamento
    if(extendedFields.exest_acabamento) $(".campos-extendidos").append('<p><b>Acabamento</b> : ' + extendedFields.exest_acabamento + '</p>');
  }